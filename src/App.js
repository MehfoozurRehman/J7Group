import React, { useEffect, useState } from "react";
import "./App.scss";
import flagshipProjectBg from "./Assets/flagshipProjectBg.png";
import logo from "./Assets/logo.png";
import jumbotronBg from "./Assets/jumbotronBg.png";
import flagshipProjectPic from "./Assets/flagshipProjectPic.png";
import projectsBg from "./Assets/projectsBg.png";
import awardedBg from "./Assets/awardedBg.png";
import awardPic from "./Assets/awardPic.png";
import trophie from "./Assets/trophie.png";
import project1 from "./Assets/logos/1.png";
import project2 from "./Assets/logos/2.png";
import project3 from "./Assets/logos/3.png";
import project4 from "./Assets/logos/4.png";
import project5 from "./Assets/logos/5.png";
import project6 from "./Assets/logos/6.png";
import ProjectPic1 from "./Assets/project_pic/1.png";
import ProjectPic2 from "./Assets/project_pic/2.png";
import ProjectPic3 from "./Assets/project_pic/3.png";
import ProjectPic4 from "./Assets/project_pic/4.png";
import teamPic1 from "./Assets/team/1.png";
import teamPic2 from "./Assets/team/2.png";
import teamPic3 from "./Assets/team/3.png";
import signatureHotel from "./Assets/projects/signatureHotel.png";
import visionBg from "./Assets/visionBg.png";
import teamsBg from "./Assets/teamsBg.png";
import visionPic from "./Assets/visionPic.png";
import section1Bg from "./Assets/section1Bg.png";
import section2Bg from "./Assets/section2Bg.png";
import section3Bg from "./Assets/section3Bg.png";
import section4Bg from "./Assets/section4Bg.png";
import j7group from "./Assets/j7group.png";
import J7Icon from "./Assets/projects/J7Icon.png";
import J7MallBack from "./Assets/projects/J7MallBack.png";
import J7MallFront from "./Assets/projects/J7MallFront.png";
import j7Emporium from "./Assets/projects/j7Emporium.png";
import { Facebook, Instagram, Linkedin, Twitter } from "react-feather";

function ContactInput({ placeholder, type, variant }) {
  if (variant === "select") {
    return (
      <div className="contact__container__form__input">
        <select
          type={type}
          placeholder={placeholder}
          id={placeholder}
          name={placeholder}
          className="contact__container__form__input__field"
        >
          <option value="">Male</option>
          <option value="">Female</option>
        </select>
        <label
          htmlFor={placeholder}
          className="contact__container__form__input__label heading"
        >
          {placeholder}
        </label>
      </div>
    );
  } else if (variant === "textarea") {
    return (
      <div
        className="contact__container__form__input"
        style={{ marginRight: "0em" }}
      >
        <textarea
          type={type}
          placeholder={placeholder}
          id={placeholder}
          name={placeholder}
          rows={5}
          className="contact__container__form__input__field"
        />
        <label
          htmlFor={placeholder}
          className="contact__container__form__input__label heading"
        >
          {placeholder}
        </label>
      </div>
    );
  } else {
    return (
      <div className="contact__container__form__input">
        <input
          type={type}
          placeholder={placeholder}
          id={placeholder}
          name={placeholder}
          className="contact__container__form__input__field"
        />
        <label
          htmlFor={placeholder}
          className="contact__container__form__input__label heading"
        >
          {placeholder}
        </label>
      </div>
    );
  }
}

function ProjectEntry({ img, onClick, defaultChecked }) {
  return (
    <div className="our__projects__container__content__entry__wrapper">
      <input
        type="radio"
        name="our__projects__container__content__entry"
        id=""
        defaultChecked={defaultChecked}
        onChange={onClick}
        className="our__projects__container__content__entry__input"
      />
      <div className="our__projects__container__content__entry">
        <img
          src={img}
          alt="project__logo"
          className="our__projects__container__content__entry__img"
        />
      </div>
    </div>
  );
}

function ProjectSwiper({ pics }) {
  useEffect(() => {
    const projects__container__overlay__content__images =
      document.getElementById("projects__container__overlay__content__images");
    projects__container__overlay__content__images.addEventListener(
      "wheel",
      (e) => {
        e.preventDefault();
        projects__container__overlay__content__images.scrollLeft += e.deltaY;
      }
    );
  }, []);
  return (
    <div
      className="projects__container__overlay__content__images"
      id="projects__container__overlay__content__images"
    >
      {pics.map((pic) => {
        return (
          <img
            src={pic.pic}
            alt="projects__container__overlay__content__image"
            className="projects__container__overlay__content__image"
          />
        );
      })}
    </div>
  );
}

export default function App() {
  const [selectedProjectTitle, setSelectedProjectTitle] =
    useState("J7 Mall Chakwall");
  const [selectedProjectInfo, setSelectedProjectInfo] = useState(
    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
  );
  const [isHeaderOpen, setIsHeaderOpen] = useState(window.innerWidth > 1000);
  useEffect(() => {
    if (window.innerWidth > 1000) {
      setIsHeaderOpen(true);
    } else {
      setIsHeaderOpen(false);
    }
    {
      window.addEventListener("resize", () => {
        if (window.innerWidth > 1000) {
          setIsHeaderOpen(true);
        } else {
          setIsHeaderOpen(false);
        }
      });
    }
  }, []);
  const [selectedProjectPics, setSelectedProjectPics] = useState([
    { pic: ProjectPic1 },
    { pic: ProjectPic2 },
    { pic: ProjectPic3 },
    { pic: ProjectPic4 },
    { pic: ProjectPic1 },
    { pic: ProjectPic2 },
    { pic: ProjectPic1 },
    { pic: ProjectPic2 },
    { pic: ProjectPic3 },
    { pic: ProjectPic4 },
    { pic: ProjectPic1 },
    { pic: ProjectPic2 },
    { pic: ProjectPic3 },
    { pic: ProjectPic4 },
    { pic: ProjectPic1 },
    { pic: ProjectPic2 },
    { pic: ProjectPic3 },
    { pic: ProjectPic4 },
  ]);
  return (
    <div className="App">
      <div className="header">
        <img src={logo} alt="logo" className="brand__logo" />
        <button
          className="menu__btn"
          onClick={() => {
            if (!isHeaderOpen) {
              setIsHeaderOpen(true);
            } else {
              setIsHeaderOpen(false);
            }
          }}
        >
          {isHeaderOpen ? (
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="24"
              height="24"
              viewBox="0 0 24 24"
              fill="none"
              stroke="currentColor"
              stroke-width="1.5"
              stroke-linecap="round"
              stroke-linejoin="round"
              class="feather feather-x"
            >
              <line x1="18" y1="6" x2="6" y2="18"></line>
              <line x1="6" y1="6" x2="18" y2="18"></line>
            </svg>
          ) : (
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="24"
              height="24"
              viewBox="0 0 24 24"
              fill="none"
              stroke="currentColor"
              stroke-width="1.5"
              stroke-linecap="round"
              stroke-linejoin="round"
              class="feather feather-menu"
            >
              <line x1="3" y1="12" x2="21" y2="12"></line>
              <line x1="3" y1="6" x2="21" y2="6"></line>
              <line x1="3" y1="18" x2="21" y2="18"></line>
            </svg>
          )}
        </button>
        {isHeaderOpen ? (
          <div className="header__nav">
            <a href="#" className="header__nav__link">
              Home
            </a>
            <a href="#" className="header__nav__link">
              About Us
            </a>
            <a href="#" className="header__nav__link">
              Projects
            </a>
            <a href="#" className="header__nav__link">
              Contact Us
            </a>
          </div>
        ) : null}
      </div>

      <div className="jumbotron__container">
        <img
          src={jumbotronBg}
          alt="jumbotronBg"
          className="jumbotron__container__img"
        />
        <div className="jumbotron__container__overlay">
          <div className="jumbotron__container__overlay__wrapper heading">
            Pakistan's best Real Estate Investment Firm
          </div>
        </div>
      </div>
      <div className="matrics__container">
        <div className="matrics__container__wrapper">
          <div className="matrics__container__card">
            <div className="matrics__container__card__value heading">7</div>
            <div className="matrics__container__card__info heading">
              <span>Cities</span>
              <span>Across</span>
              <span>Pakistan</span>
            </div>
          </div>
          <div className="matrics__container__card">
            <div className="matrics__container__card__value heading">4</div>
            <div className="matrics__container__card__info heading">
              <span>National</span>
              <span>Level</span>
              <span>Awards</span>
            </div>
          </div>
          <div className="matrics__container__card">
            <div className="matrics__container__card__value heading">8</div>
            <div className="matrics__container__card__info heading">
              <span>Completed</span>
              <span>Mega</span>
              <span>Structures</span>
            </div>
          </div>
          <div className="matrics__container__card__reverse">
            <div className="matrics__container__card__reverse__info heading">
              Happy Customers
            </div>
            <div className="matrics__container__card__reverse__value heading">
              10k+
            </div>
          </div>
        </div>
      </div>
      <div className="flagship__project__container">
        <img
          src={flagshipProjectBg}
          alt="flagshipProjectBg"
          className="flagship__project__container__img"
        />
        <div className="flagship__project__container__overlay">
          <div className="flagship__project__container__overlay__wrapper">
            <div className="flagship__project__container__overlay__left">
              <div className="flagship__project__container__overlay__left__heading heading">
                <span>FLAGSHIP</span> PROJECT
              </div>
              <div className="flagship__project__container__overlay__left__info">
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book. It has
                survived not only five centuries, but also the leap into
                electronic typesetting, remaining essentially unchanged. It was
                popularised in the 1960s with the release of Letraset sheets
                containing Lorem Ipsum passages, and more recently with desktop
                publishing software like Aldus PageMaker including versions of
                Lorem Ipsum.
              </div>
            </div>
            <img
              src={flagshipProjectPic}
              alt="flagshipProjectPic"
              className="flagship__project__container__overlay__right"
            />
          </div>
        </div>
        <div className="our__projects__container">
          <div className="our__projects__container__heading heading">
            Our <span>Projects</span>
          </div>
          <div className="our__projects__container__content">
            <ProjectEntry
              img={project1}
              defaultChecked={true}
              onClick={() => {
                setSelectedProjectTitle("J7 Mall Chakwall");
                setSelectedProjectInfo(
                  "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
                );
                setSelectedProjectPics([
                  { pic: ProjectPic1 },
                  { pic: ProjectPic2 },
                  { pic: ProjectPic3 },
                  { pic: ProjectPic4 },
                  { pic: ProjectPic1 },
                  { pic: ProjectPic2 },
                  { pic: ProjectPic1 },
                  { pic: ProjectPic2 },
                  { pic: ProjectPic3 },
                  { pic: ProjectPic4 },
                  { pic: ProjectPic1 },
                  { pic: ProjectPic2 },
                  { pic: ProjectPic3 },
                  { pic: ProjectPic4 },
                  { pic: ProjectPic1 },
                  { pic: ProjectPic2 },
                  { pic: ProjectPic3 },
                  { pic: ProjectPic4 },
                ]);
              }}
            />
            <ProjectEntry
              img={project2}
              onClick={() => {
                setSelectedProjectTitle("J7 Icon");
                setSelectedProjectInfo(
                  "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s"
                );
                setSelectedProjectPics([
                  { pic: ProjectPic1 },
                  { pic: ProjectPic2 },
                  { pic: ProjectPic3 },
                  { pic: ProjectPic4 },
                  { pic: ProjectPic1 },
                  { pic: ProjectPic2 },
                  { pic: ProjectPic1 },
                  { pic: ProjectPic2 },
                  { pic: ProjectPic3 },
                  { pic: ProjectPic4 },
                  { pic: ProjectPic1 },
                  { pic: ProjectPic2 },
                  { pic: ProjectPic3 },
                  { pic: ProjectPic4 },
                  { pic: ProjectPic1 },
                  { pic: ProjectPic2 },
                  { pic: ProjectPic3 },
                  { pic: ProjectPic4 },
                ]);
              }}
            />
            <ProjectEntry
              img={project3}
              onClick={() => {
                setSelectedProjectTitle("J7 Global");
                setSelectedProjectInfo(
                  "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the "
                );
                setSelectedProjectPics([
                  { pic: ProjectPic1 },
                  { pic: ProjectPic2 },
                  { pic: ProjectPic3 },
                  { pic: ProjectPic4 },
                  { pic: ProjectPic1 },
                  { pic: ProjectPic2 },
                  { pic: ProjectPic1 },
                  { pic: ProjectPic2 },
                  { pic: ProjectPic3 },
                  { pic: ProjectPic4 },
                  { pic: ProjectPic1 },
                  { pic: ProjectPic2 },
                  { pic: ProjectPic3 },
                  { pic: ProjectPic4 },
                  { pic: ProjectPic1 },
                  { pic: ProjectPic2 },
                  { pic: ProjectPic3 },
                  { pic: ProjectPic4 },
                ]);
              }}
            />
            <ProjectEntry
              img={project4}
              onClick={() => {
                setSelectedProjectTitle("J7 Mall");
                setSelectedProjectInfo(
                  "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged."
                );
                setSelectedProjectPics([
                  { pic: ProjectPic1 },
                  { pic: ProjectPic2 },
                  { pic: ProjectPic3 },
                  { pic: ProjectPic4 },
                  { pic: ProjectPic1 },
                  { pic: ProjectPic2 },
                  { pic: ProjectPic1 },
                  { pic: ProjectPic2 },
                  { pic: ProjectPic3 },
                  { pic: ProjectPic4 },
                  { pic: ProjectPic1 },
                  { pic: ProjectPic2 },
                  { pic: ProjectPic3 },
                  { pic: ProjectPic4 },
                  { pic: ProjectPic1 },
                  { pic: ProjectPic2 },
                  { pic: ProjectPic3 },
                  { pic: ProjectPic4 },
                ]);
              }}
            />
            <ProjectEntry
              img={project5}
              onClick={() => {
                setSelectedProjectTitle("J7 Mall One");
                setSelectedProjectInfo(
                  "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,"
                );
                setSelectedProjectPics([
                  { pic: ProjectPic1 },
                  { pic: ProjectPic2 },
                  { pic: ProjectPic3 },
                  { pic: ProjectPic4 },
                  { pic: ProjectPic1 },
                  { pic: ProjectPic2 },
                  { pic: ProjectPic1 },
                  { pic: ProjectPic2 },
                  { pic: ProjectPic3 },
                  { pic: ProjectPic4 },
                  { pic: ProjectPic1 },
                  { pic: ProjectPic2 },
                  { pic: ProjectPic3 },
                  { pic: ProjectPic4 },
                  { pic: ProjectPic1 },
                  { pic: ProjectPic2 },
                  { pic: ProjectPic3 },
                  { pic: ProjectPic4 },
                ]);
              }}
            />
            <ProjectEntry
              img={project6}
              onClick={() => {
                setSelectedProjectTitle("Sign Marketing");
                setSelectedProjectInfo(
                  "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
                );
                setSelectedProjectPics([
                  { pic: ProjectPic1 },
                  { pic: ProjectPic2 },
                  { pic: ProjectPic3 },
                  { pic: ProjectPic4 },
                  { pic: ProjectPic1 },
                  { pic: ProjectPic2 },
                  { pic: ProjectPic1 },
                  { pic: ProjectPic2 },
                  { pic: ProjectPic3 },
                  { pic: ProjectPic4 },
                  { pic: ProjectPic1 },
                  { pic: ProjectPic2 },
                  { pic: ProjectPic3 },
                  { pic: ProjectPic4 },
                  { pic: ProjectPic1 },
                  { pic: ProjectPic2 },
                  { pic: ProjectPic3 },
                  { pic: ProjectPic4 },
                ]);
              }}
            />
          </div>
        </div>
        <div className="projects__container">
          <img
            src={projectsBg}
            alt="projectsBg"
            className="projects__container__img"
          />
          <div className="projects__container__overlay">
            <div className="projects__container__overlay__content">
              <div className="projects__container__overlay__content__heading heading">
                {selectedProjectTitle}
              </div>
              <div className="projects__container__overlay__content__info">
                {selectedProjectInfo}
              </div>
              <ProjectSwiper pics={selectedProjectPics} />
            </div>
          </div>
        </div>
        <div className="awarded__project__container">
          <img
            src={awardedBg}
            alt="awardedBg"
            className="awarded__project__container__img"
          />
          <div className="awarded__project__container__overlay">
            <div className="awarded__project__container__overlay__content">
              <div className="awarded__project__container__overlay__content__left">
                <div className="awarded__project__container__overlay__content__left__heading heading">
                  <span>Awarded</span> PROJECT
                </div>
                <div className="awarded__project__container__overlay__content__left__info">
                  Lorem Ipsum is simply dummy text of the printing and
                  typesetting industry. Lorem Ipsum has been the industry's
                  standard dummy text ever since the 1500s, when an unknown
                  printer took a galley of type and scrambled it to make a type
                  specimen book. It has survived not only five centuries, but
                  also the leap into electronic typesetting, remaining
                  essentially unchanged. It was popularised in the 1960s with
                  the release of Letraset sheets containing Lorem Ipsum
                  passages, and more recently with desktop publishing software
                  like Aldus PageMaker including versions of Lorem Ipsum.
                </div>
              </div>
              <img
                src={trophie}
                alt="trophie"
                className="awarded__project__container__overlay__content__middle"
              />
              <img
                src={awardPic}
                alt="awardPic"
                className="awarded__project__container__overlay__content__right"
              />
            </div>
          </div>
        </div>
        <div className="main__projects__container">
          <img
            src={section2Bg}
            alt="section1Bg"
            className="main__projects__container__img"
          />
          <div className="main__projects__container__overlay">
            <div className="main__projects__container__overlay__content">
              <img
                src={signatureHotel}
                alt="signatureHotel"
                className="main__projects__container__overlay__content__left"
              />
              <div className="main__projects__container__overlay__content__right">
                <div className="main__projects__container__overlay__content__right__bar"></div>
                <div className="main__projects__container__overlay__content__right__content">
                  <div className="main__projects__container__overlay__content__right__content__heading heading">
                    <span>Signature</span> Hotel
                  </div>
                  <div className="main__projects__container__overlay__content__right__content__info">
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry. Lorem Ipsum has been the industry's
                    standard dummy text ever since the 1500s, when an unknown
                    printer took a galley of type and scrambled it to make a
                    type specimen book. It has survived not only five centuries,
                    but also the leap into electronic typesetting, remaining
                    essentially unchanged. It was popularised in the 1960s with
                    the release of Letraset sheets containing Lorem Ipsum
                    passages, and more recently with desktop publishing software
                    like Aldus PageMaker including versions of Lorem Ipsum.
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry. Lorem Ipsum has been the industry's
                    standard dummy text ever since the 1500s, when an unknown
                    printer took a galley of type and scrambled it to make a
                    type specimen book. It has survived not only five centuries,
                    but also the leap into electronic typesetting, remaining
                    essentially unchanged. It was popularised in the 1960s with
                    the release of Letraset sheets containing Lorem Ipsum
                    passages, and more recently with desktop publishing software
                    like Aldus PageMaker including versions of Lorem Ipsum.
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry. Lorem Ipsum has been the industry's
                    standard dummy text ever since the 1500s, when an unknown
                    printer took a galley of type and scrambled it to make a
                    type specimen book. It has survived not only five centuries,
                    but also the leap into electronic typesetting, remaining
                    essentially unchanged. It was popularised in the 1960s with
                    the release of Letraset sheets containing Lorem Ipsum
                    passages, and more recently with desktop publishing software
                    like Aldus PageMaker including versions of Lorem Ipsum.
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="main__projects__container ">
          <img
            src={section4Bg}
            alt="section1Bg"
            className="main__projects__container__img"
          />
          <div className="main__projects__container__overlay">
            <div className="main__projects__container__overlay__content main__projects__container__overlay__content__reverse">
              <div className="main__projects__container__overlay__content__right">
                <div className="main__projects__container__overlay__content__right__bar"></div>
                <div className="main__projects__container__overlay__content__right__content">
                  <div className="main__projects__container__overlay__content__right__content__heading heading">
                    <span>J7</span> ICON
                  </div>
                  <div className="main__projects__container__overlay__content__right__content__info">
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry. Lorem Ipsum has been the industry's
                    standard dummy text ever since the 1500s, when an unknown
                    printer took a galley of type and scrambled it to make a
                    type specimen book. It has survived not only five centuries,
                    but also the leap into electronic typesetting, remaining
                    essentially unchanged. It was popularised in the 1960s with
                    the release of Letraset sheets containing Lorem Ipsum
                    passages, and more recently with desktop publishing software
                    like Aldus PageMaker including versions of Lorem Ipsum.
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry. Lorem Ipsum has been the industry's
                    standard dummy text ever since the 1500s, when an unknown
                    printer took a galley of type and scrambled it to make a
                    type specimen book. It has survived not only five centuries,
                    but also the leap into electronic typesetting, remaining
                    essentially unchanged. It was popularised in the 1960s with
                    the release of Letraset sheets containing Lorem Ipsum
                    passages, and more recently with desktop publishing software
                    like Aldus PageMaker including versions of Lorem Ipsum.
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry. Lorem Ipsum has been the industry's
                    standard dummy text ever since the 1500s, when an unknown
                    printer took a galley of type and scrambled it to make a
                    type specimen book. It has survived not only five centuries,
                    but also the leap into electronic typesetting, remaining
                    essentially unchanged. It was popularised in the 1960s with
                    the release of Letraset sheets containing Lorem Ipsum
                    passages, and more recently with desktop publishing software
                    like Aldus PageMaker including versions of Lorem Ipsum.
                  </div>
                </div>
              </div>
              <img
                src={J7Icon}
                alt="J7Icon"
                className="main__projects__container__overlay__content__left"
              />
            </div>
          </div>
        </div>
        <div className="main__projects__container">
          <img
            src={section3Bg}
            alt="section1Bg"
            className="main__projects__container__img"
          />
          <div className="main__projects__container__overlay">
            <div className="main__projects__container__overlay__content">
              <img
                src={J7MallBack}
                alt="J7MallBack"
                className="main__projects__container__overlay__content__left"
              />
              <div className="main__projects__container__overlay__content__right">
                <div className="main__projects__container__overlay__content__right__bar"></div>
                <div className="main__projects__container__overlay__content__right__content">
                  <div className="main__projects__container__overlay__content__right__content__heading heading">
                    <span>J7</span> Mall
                  </div>
                  <div className="main__projects__container__overlay__content__right__content__info">
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry. Lorem Ipsum has been the industry's
                    standard dummy text ever since the 1500s, when an unknown
                    printer took a galley of type and scrambled it to make a
                    type specimen book. It has survived not only five centuries,
                    but also the leap into electronic typesetting, remaining
                    essentially unchanged. It was popularised in the 1960s with
                    the release of Letraset sheets containing Lorem Ipsum
                    passages, and more recently with desktop publishing software
                    like Aldus PageMaker including versions of Lorem Ipsum.
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry. Lorem Ipsum has been the industry's
                    standard dummy text ever since the 1500s, when an unknown
                    printer took a galley of type and scrambled it to make a
                    type specimen book. It has survived not only five centuries,
                    but also the leap into electronic typesetting, remaining
                    essentially unchanged. It was popularised in the 1960s with
                    the release of Letraset sheets containing Lorem Ipsum
                    passages, and more recently with desktop publishing software
                    like Aldus PageMaker including versions of Lorem Ipsum.
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry. Lorem Ipsum has been the industry's
                    standard dummy text ever since the 1500s, when an unknown
                    printer took a galley of type and scrambled it to make a
                    type specimen book. It has survived not only five centuries,
                    but also the leap into electronic typesetting, remaining
                    essentially unchanged. It was popularised in the 1960s with
                    the release of Letraset sheets containing Lorem Ipsum
                    passages, and more recently with desktop publishing software
                    like Aldus PageMaker including versions of Lorem Ipsum.
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="main__projects__container">
          <img
            src={section1Bg}
            alt="section1Bg"
            className="main__projects__container__img"
          />
          <div className="main__projects__container__overlay">
            <div className="main__projects__container__overlay__content main__projects__container__overlay__content__reverse">
              <div className="main__projects__container__overlay__content__right">
                <div className="main__projects__container__overlay__content__right__bar"></div>
                <div className="main__projects__container__overlay__content__right__content">
                  <div className="main__projects__container__overlay__content__right__content__heading heading">
                    <span>J7</span> Mall
                  </div>
                  <div className="main__projects__container__overlay__content__right__content__info">
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry. Lorem Ipsum has been the industry's
                    standard dummy text ever since the 1500s, when an unknown
                    printer took a galley of type and scrambled it to make a
                    type specimen book. It has survived not only five centuries,
                    but also the leap into electronic typesetting, remaining
                    essentially unchanged. It was popularised in the 1960s with
                    the release of Letraset sheets containing Lorem Ipsum
                    passages, and more recently with desktop publishing software
                    like Aldus PageMaker including versions of Lorem Ipsum.
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry. Lorem Ipsum has been the industry's
                    standard dummy text ever since the 1500s, when an unknown
                    printer took a galley of type and scrambled it to make a
                    type specimen book. It has survived not only five centuries,
                    but also the leap into electronic typesetting, remaining
                    essentially unchanged. It was popularised in the 1960s with
                    the release of Letraset sheets containing Lorem Ipsum
                    passages, and more recently with desktop publishing software
                    like Aldus PageMaker including versions of Lorem Ipsum.
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry. Lorem Ipsum has been the industry's
                    standard dummy text ever since the 1500s, when an unknown
                    printer took a galley of type and scrambled it to make a
                    type specimen book. It has survived not only five centuries,
                    but also the leap into electronic typesetting, remaining
                    essentially unchanged. It was popularised in the 1960s with
                    the release of Letraset sheets containing Lorem Ipsum
                    passages, and more recently with desktop publishing software
                    like Aldus PageMaker including versions of Lorem Ipsum.
                  </div>
                </div>
              </div>
              <img
                src={J7MallFront}
                alt="J7MallFront"
                className="main__projects__container__overlay__content__left"
              />
            </div>
          </div>
        </div>
        <div className="booking__container">
          <img
            src={section4Bg}
            alt="section1Bg"
            className="booking__container__img"
          />
          <div className="booking__container__overlay">
            <div className="booking__container__overlay__content">
              <div className="booking__container__overlay__content__left">
                <div className="booking__container__overlay__content__left__heading heading">
                  <span>bookings</span>
                  <br />
                  sTARTS fROM
                  <br />
                  <span>25%</span>
                </div>
              </div>
              <img
                src={j7Emporium}
                alt="j7Emporium"
                className="booking__container__overlay__content__middle"
              />
              <div className="booking__container__overlay__content__right">
                <img
                  src={j7group}
                  alt="j7group"
                  className="booking__container__overlay__content__right__img"
                />
              </div>
            </div>
          </div>
        </div>
        <div className="vision__container">
          <img
            src={visionBg}
            alt="visionBg"
            className="vision__container__img"
          />
          <div className="vision__container__overlay">
            <div className="vision__container__overlay__content">
              <div className="vision__container__overlay__content__left">
                <div className="vision__container__overlay__content__left__heading heading">
                  <span>OUR VISION</span> <br />
                  IS YOUR VISION
                </div>
                <div className="vision__container__overlay__content__left__info">
                  Lorem Ipsum is simply dummy text of the printing and
                  typesetting industry. Lorem Ipsum has been the industry's
                  standard dummy text ever since the 1500s, when an unknown
                  printer took a galley of type and scrambled it to make a type
                  specimen book. It has survived not only five centuries, but
                  also the leap into electronic typesetting,
                </div>
              </div>
              <img
                src={visionPic}
                alt="visionPic"
                className="vision__container__overlay__content__right"
              />
            </div>
          </div>
        </div>
        <iframe
          src="https://www.youtube.com/embed/5ANSa-hJtXU"
          title="YouTube video player"
          frameborder="0"
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          allowfullscreen
          className="youtube__video"
        ></iframe>
        <div className="team__container">
          <img src={teamsBg} alt="teamsBg" className="team__container__img" />
          <div className="team__container__overlay">
            <div className="team__container__overlay__content">
              <div className="team__container__overlay__content__heading heading">
                J7 Team
              </div>
              <div className="team__container__overlay__wrapper">
                <div className="team__container__overlay__content__card">
                  <img
                    src={teamPic1}
                    alt="Mr. Maqbool Hussain"
                    className="team__container__overlay__content__card__img"
                  />
                  <div className="team__container__overlay__content__card__name heading">
                    Mr. Maqbool Hussain
                  </div>
                  <div className="team__container__overlay__content__card__designation">
                    Chairman
                  </div>
                </div>
                <div className="team__container__overlay__content__card">
                  <img
                    src={teamPic2}
                    alt="Mr. Maqbool Hussain"
                    className="team__container__overlay__content__card__img"
                  />
                  <div className="team__container__overlay__content__card__name heading">
                    Mr. Yaseen Masud
                  </div>
                  <div className="team__container__overlay__content__card__designation">
                    C.E.O
                  </div>
                </div>
                <div className="team__container__overlay__content__card">
                  <img
                    src={teamPic3}
                    alt="Mr. Maqbool Hussain"
                    className="team__container__overlay__content__card__img"
                  />
                  <div className="team__container__overlay__content__card__name heading">
                    Mr. Aleem Malik
                  </div>
                  <div className="team__container__overlay__content__card__designation">
                    Director
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="contact__container">
          <div className="contact__container__content">
            <iframe
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3307.5173288665815!2d71.50270921519028!3d34.00492952766938!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38d917aa3edd1ed3%3A0x974d74f7f91268e9!2sJ7%20Group%20Peshawar!5e0!3m2!1sen!2s!4v1635106554642!5m2!1sen!2s"
              allowfullscreen=""
              loading="lazy"
            ></iframe>
            <form action="" className="contact__container__form">
              <div className="contact__container__form__heading heading">
                <span>Get in</span> Touch
              </div>
              <div className="contact__container__form__info">
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the
              </div>
              <div className="contact__container__form__row">
                <ContactInput placeholder="first name" type="text" />
                <ContactInput placeholder="last name" type="text" />
              </div>
              <div className="contact__container__form__row">
                <ContactInput placeholder="email" type="email" />
                <ContactInput placeholder="DOB" type="text" />
              </div>
              <div className="contact__container__form__row">
                <ContactInput placeholder="phone" type="tel" />
                <ContactInput placeholder="gender" variant="select" />
              </div>
              <div className="contact__container__form__row">
                <ContactInput placeholder="Message" variant="textarea" />
              </div>
              <button className="contact__container__form__button">Send</button>
              <div className="contact__container__form__heading heading">
                <span>Contact</span> Us
              </div>
              <div className="contact__container__form__content">
                <div className="contact__container__form__content__col">
                  <div className="contact__container__form__content__col__heading heading">
                    Phone
                  </div>
                  <div className="contact__container__form__content__col__value">
                    335 353 343
                  </div>
                  <div className="contact__container__form__content__col__value">
                    335 353 343
                  </div>
                </div>
                <div className="contact__container__form__content__col">
                  <div className="contact__container__form__content__col__heading heading">
                    Email
                  </div>
                  <div className="contact__container__form__content__col__value">
                    hello@J7.com
                  </div>
                  <div className="contact__container__form__content__col__value">
                    hello@J7Group.com
                  </div>
                </div>
                <div className="contact__container__form__content__col">
                  <div className="contact__container__form__content__col__heading heading">
                    Socials
                  </div>
                  <div className="contact__container__form__content__col__links">
                    <a
                      href="#"
                      className="contact__container__form__content__col__link"
                    >
                      <Facebook size={18} color="currentColor" />
                    </a>
                    <a
                      href="#"
                      className="contact__container__form__content__col__link"
                    >
                      <Instagram size={18} color="currentColor" />
                    </a>
                    <a
                      href="#"
                      className="contact__container__form__content__col__link"
                    >
                      <Twitter size={18} color="currentColor" />
                    </a>
                    <a
                      href="#"
                      className="contact__container__form__content__col__link"
                    >
                      <Linkedin size={18} color="currentColor" />
                    </a>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}
